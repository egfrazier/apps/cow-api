import hvac

def get_secret(path: str) -> dict:
    client = hvac.Client()
    return client.secrets.kv.read_secret_version(path=path)