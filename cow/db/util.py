from flask.cli import with_appcontext

from cow.db import close_db
from cow.db.cli import (
    init_db_command,
    load_db_command,
    psql
)


# TODO: Centralize where the commands get
# registered with the app
def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(load_db_command)
    app.cli.add_command(psql)

