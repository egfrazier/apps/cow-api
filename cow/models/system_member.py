from cow.db import get_db

db = get_db()

class SystemMember(db.Model):
    __tablename__ = 'system_members'

    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.String, unique=False, nullable=False)
    ccode = db.Column(db.Integer, unique=False, nullable=False)
    abbr = db.Column(db.String, unique=False, nullable=False)

    def __repr__(self):
        return f'<SystemMember {self.abbr} - {self.year}>'