from cow.db import get_db

db = get_db()

class State(db.Model):
    __tablename__ = 'states'

    id = db.Column(db.Integer, primary_key=True)
    ccode = db.Column(db.Integer, unique=True, nullable=False)
    abbr = db.Column(db.String, unique=True, nullable=False)
    name = db.Column(db.String, unique=True, nullable=False)

    def __repr__(self):
        return f'<State {self.name}>'
