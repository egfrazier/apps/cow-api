import csv
from urllib.request import urlopen

from flask import current_app

from cow.db import get_db

db = get_db()

from cow.models import State
from cow.models import SystemMember


def _fetch_data(url):
    resp = urlopen(url)
    raw_contents = resp.read().decode('utf-8').split('\r')
    return raw_contents


def load_states(source):
    lines = [l.strip('\n') for l in _fetch_data(source)]
    line_reader = csv.DictReader(lines, delimiter=',')
    
    state_codes = []
    
    for row in line_reader:
        if row['CCode'] in state_codes:
            continue

        if (existing_state := State.query.filter_by(ccode=row['CCode']).one_or_none()):
            continue

        state = State(
            ccode=row['CCode'],
            abbr=row['StateAbb'],
            name=row['StateNme'],
        )
        db.session.add(state)
        state_codes.append(row['CCode'])
    
    db.session.commit()


def load_system_members(source):
    lines = [l.strip('\n') for l in _fetch_data(source)]
    line_reader = csv.DictReader(lines, delimiter=',')
    
    for row in line_reader:
        existing_member = SystemMember.query.filter_by(
            ccode=row['ccode'],
            abbr=row['stateabb'],
            year=row['year']
        ).one_or_none()

        if existing_member:
            continue

        system_member = SystemMember(
            ccode=row['ccode'],
            abbr=row['stateabb'],
            year=row['year']
        )
        db.session.add(system_member)
    
    db.session.commit()


def run():
    load_states(
        current_app.config['DATA_SOURCES']['COUNTRY_CODES']
    )
    load_system_members(
        current_app.config['DATA_SOURCES']['SYSTEM_MEMBERS']
    )

if __name__ == '__main__':
    run()