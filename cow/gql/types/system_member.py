from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType

from cow.models import SystemMember as SystemMemberModel

class SystemMember(SQLAlchemyObjectType):
    class Meta:
        model = SystemMemberModel
        interfaces = (relay.Node, )
