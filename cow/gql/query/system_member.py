from graphene import ObjectType, Int, String, Schema, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField

from cow.gql.types.system_member import SystemMember as SystemMemberNode

class SystemMemberQuery(ObjectType):
    all_system_members = SQLAlchemyConnectionField(SystemMemberNode.connection)


