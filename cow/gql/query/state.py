from graphene import ObjectType, Int, String, Schema, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField

from cow.gql.types.state import State as StateNode
from cow.models import State as StateModel

def _get_state(country_code):
    s = StateModel.query.filter_by(ccode=country_code).first()
    return {
        'ccode': s.ccode,
        'abbr': s.abbr,
        'name': s.name
    }

class StateQuery(ObjectType):
    state = String(ccode=Int(default_value=2))

    def resolve_state(root, info, ccode):
        return _get_state(ccode)

    all_states = SQLAlchemyConnectionField(StateNode.connection)


