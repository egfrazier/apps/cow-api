import os
import tempfile

import sys
sys.path.append('..')

import pytest

from cow import create_app
from cow.db import get_db
from cow.db.cli import init_db

@pytest.fixture
def client():
    app = create_app({'TESTING': True})

    with app.test_client() as client:
        with app.app_context():
            init_db()
        yield client


def test_hello(client):
    rv = client.get('/')
    assert b'Correlates of War API' in rv.data
